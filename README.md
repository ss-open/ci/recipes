# CI Recipes

- [CI Recipes](#ci-recipes)
  - [Configuration](#configuration)
    - [Automatic jobs](#automatic-jobs)
    - [Exclude a job](#exclude-a-job)
    - [Partial include](#partial-include)
  - [Docker workflow](#docker-workflow)
    - [Project setup](#project-setup)
      - [`.env.dist`](#envdist)
      - [`compose.yaml`](#composeyaml)
    - [GitLab CI configuration](#gitlab-ci-configuration)
      - [Configure the test service to use](#configure-the-test-service-to-use)
    - [Deployment job specific runner](#deployment-job-specific-runner)
    - [Templates](#templates)
  - [GitLab Access token with write permissions](#gitlab-access-token-with-write-permissions)
  - [Custom jobs using templates](#custom-jobs-using-templates)
    - [Note about independent jobs with the `interruptible` configuration key](#note-about-independent-jobs-with-the-interruptible-configuration-key)
  - [Task jobs definition for scheduled CI](#task-jobs-definition-for-scheduled-ci)
    - [Configure a task job](#configure-a-task-job)
    - [Configure the schedule pipeline](#configure-the-schedule-pipeline)
  - [Include methods for projects outside of gitlab.com](#include-methods-for-projects-outside-of-gitlabcom)
    - [Using the `remote` include (recommended)](#using-the-remote-include-recommended)
    - [Project mirroring](#project-mirroring)
      - [Create a project for mirroring](#create-a-project-for-mirroring)
      - [Enabled project mirroring](#enabled-project-mirroring)
      - [Use the mirrored repository](#use-the-mirrored-repository)
  - [Create your own custom CI entrypoint](#create-your-own-custom-ci-entrypoint)
  - [Job specific documentation](#job-specific-documentation)
  - [Troubleshooting](#troubleshooting)
    - [`!reference ["***"] could not be found` or `unknown keys in extends (***)`](#reference--could-not-be-found-or-unknown-keys-in-extends-)
    - [The npm vendor cache creation is always being killed](#the-npm-vendor-cache-creation-is-always-being-killed)

## Configuration

Note: `vX.Y.Z` refers to the latest pushed release on that project.

### Automatic jobs

To get all the automatic CI job being set, include the following:

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: auto.gitlab-ci.yml }
```

### Exclude a job

If you don't want to exclude a job, you may deactivate it using the `rules` keyword:

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: auto.gitlab-ci.yml }

# I want to do dirty bash. Leave me alone, shellcheck!
lint:shellcheck:
  rules:
    - when: never
```

You may also use the YAML anchors' syntax to disable multiple jobs with ease:

```yaml
.never: &never
  rules:
    - when: never

lint:shellcheck: *never
lint:eslint: *never
```

It is the recommended way to still automatically get potential new tools from the future releases!

### Partial include

You may prefer to include only a part of the automatic jobs.

In order to get the independently included jobs working properly, the
[`init.gitlab-ci.yml`](init.gitlab-ci.yml)
file **MUST be included** first, replacing
[`auto.gitlab-ci.yml`](auto.gitlab-ci.yml).

Then, you can include only the stages or the jobs you desire:

```yaml
include:
  # To be included in place of the auto.gitlab-ci.yml file.
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: init.gitlab-ci.yml }
  # Directly include the stages and jobs you want.
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/checkov.gitlab-ci.yml
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/eslint.gitlab-ci.yml
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/hadolint.gitlab-ci.yml
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/yamllint.gitlab-ci.yml
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/verify.gitlab-ci.yml
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/release.gitlab-ci.yml
```

Note: You can find out the default jobs references by looking at the root file of the [stages](./stages) folder.

## Docker workflow

The `ss-open/ci` project provides a Docker based CI/CD workflow that can be implemented following a few steps defined below.

### Project setup

In order to use our Docker based workflow, you need at minimum to provide the files defined on the sections below.

Also, your `.gitignore` file MUST contains at least the following:

```.gitignore
/.env
```

#### `.env.dist`

Provides the default environment variable for the Docker processes.

```env
IMAGE_NAME=my/project/namespace-slug
IMAGE_TAG=latest
COMPOSE_FILE=compose.yaml:compose.dev.yaml
```

Note: In order to be effective, this file MUST be copied to `.env` before running any `docker compose` command.

#### `compose.yaml`

Any job of the Docker workflow use `docker compose` commands and need at least a `compose.yaml` file as follow:

```yaml
version: '3'

services:
  # The main service. By default, it is the only one up by the deployment job.
  app:
    # The variables are build automatically based on the project's namespace.
    image: ${IMAGE_NAME}:${IMAGE_TAG}
    build:
      context: .
      # Configure a local cache to be created and used by the CI job with buildx.
      x-bake:
        cache-from:
          - type=local,src=.cache/docker
        cache-to:
          - type=local,dest=.cache/docker
    # Important to get the related services up and running during the deployment job.
    depends_on:
      - service-1
      - service-2
      - ...

  # Needed if you want to setup the test stage.
  test:
    extends: app
    command: my-test-command

  # Needed if you want to setup the publish stage.
  publish:
    extends: app
    command: my-publish-command
```

It is also possible to provide additional `docker-compose.*.yml` file depending of the application scope:

- `compose.ci.yaml`: This configuration is **included by default** by any `.docker` based CI job
  and MUST be provided to work properly **unless you change it** like described above.
- `compose.deploy.yaml`: Included automatically by the jobs of the `deploy` stage if the file is present.
- `compose.dev.yaml`: Ignored, for local usage only.

To override the default included configuration file for the docker based job,
you can play with the `COMPOSE_FILE` variable like so:

```yaml
# The base template for any docker workflow based job.
.docker:
  variables:
    COMPOSE_FILE: compose.yaml:compose.ci.yaml
```

### GitLab CI configuration

Once the project setup, simply include the workflow stage you need on your `.gitlab-ci.yml` configuration file:

```yaml
include:
  # Or auto.gitlab-ci.yml.
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: init.gitlab-ci.yml }
  # The docker workflow part.
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/build.gitlab-ci.yml }
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/test.gitlab-ci.yml }
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/deploy.gitlab-ci.yml }

.workflow:changes:
  - .gitlab-ci.yml # Recommended to verify the workflow on CI configuration update.
  - 'src/**/*'
```

Notes:

- Each stage is independent and can be included without the others.
- The `.workflow:changes` defines the matched changes to trigger the review workflow on merge requests.
  See the [official documentation](https://docs.gitlab.com/ee/ci/yaml/#ruleschanges)
  about the `rules:changes` keyword for details.

#### Configure the test service to use

You MAY want to change the test service name in order to create a new specific test job.
You can do eat by overriding the `SS_CI_RECIPES_DOCKER_TEST_IMAGE` variable.

Here is an example to setup a test job named `test:chromatic`:

```yaml
test:chromatic:
  extends: .test
  variables:
    SS_CI_RECIPES_DOCKER_TEST_IMAGE: chromatic
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    # Add this rule to run the job against the main branch (staging)
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: !reference [.workflow:changes]
    # Add this rule to run the job against your merge request (review)
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes: !reference [.workflow:changes]
```

With this configuration, the test job will manipulate the `chromatic` service instead of `test` which is the default.

### Deployment job specific runner

Currently, the docker deployment job is a simple `docker compose up`
command that MAY need to be executed on a dedicated server.

On that case, you have to register a runner on it with a specific tag. Then, specify it on the related deploy jobs:

```yaml
# Default tag for any deployment job.
.deploy:
  tags:
    - deploy

# Production specific override.
deploy:production
  tags:
    - deploy:production
```

### Templates

The samples above are just here to show you the necessary basics, but should be optimized according to your needs.

As the possible enhancements hereby depend of the project kind
and are more related to knowledge regarding Docker, they won't be listed here.

However, you can get some inspiration by looking at the [templates we propose](https://gitlab.com/ss-open/templates).

## GitLab Access token with write permissions

Some CI jobs needs write access to your project to proceed successfully, like for the [release stage](./stages/release).

Unfortunately, it is currently not possible to use the
[generated `CI_JOB_TOKEN`](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html)
because it does not have any write permission.

This subject is under discussion here: <https://gitlab.com/gitlab-org/gitlab/-/issues/389060>

Jobs with write permission requirement use the `GITLAB_TOKEN` variable you must provide using your
[project or group CI settings](https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui).
We hereby recommend to **make it accessible only on protected environments** and
[secure it correctly](https://docs.gitlab.com/ee/ci/variables/#cicd-variable-security).

The token can be from any account as soon as it as the write permission to the related project or group.
It may be a good idea to create a separated and restricted **bot account**
to differentiate automatic modifications from manual ones.

## Custom jobs using templates

You may be interested by our job context setup processes for your own CI jobs.

Hopefully, you can re-use them with ease using our templates. Here is some examples:

```yaml
include:
  # or auto.gitlab-ci.yml
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: init.gitlab-ci.yml }

job:docker:
  extends:
    - .docker
  script:
    # Include the context setup scripts by referencing them at the very beginning.
    - !reference [.docker, script]
    - docker compose exec -T app my-containered-command

job:node:
  extends:
    - .node
  script:
    - !reference [.node, script]
    - node my-script.js

job:npm:
  extends:
    # The .node template is included by .node:install itself and is not necessary here.
    - .node:install
  script:
    - !reference [.node, script]
    - !reference [.node:install, script]
    - npm run my-task
```

You can find them all on the [`templates`](./templates)
folder and additional working sample by walking trough the job defines on the [`stages`](./stages) folder.

### Note about independent jobs with the `interruptible` configuration key

You may want to create independent jobs, such as a custom lint job.

Those can of jobs are generally independent and can be interrupted.
On that case, we recommend the following configuration:

```yaml
lint:custom-linter:
  stages: lint
  needs: []
  interruptible: true
```

Note the importance of having `interruptible: true` being set.
If you don't, the start of this job will prevents the cancellation
of all the other jobs of the current stages and the previous one.

As the job is started at the very beginning, not having the flag results to prevents all the cancellation system.

See also the following issue: <https://gitlab.com/gitlab-org/gitlab-runner/-/issues/35998>

## Task jobs definition for scheduled CI

This project defines a `task` stage by default.
It currently has no internal usage but is recommended to be used
for any repeated task you want to configure trough a CI job.

The following explain how to configure a task job. It is a recommended but not mandatory way to do it.

We recommend to keep the same scheduled CI configuration across your project to make CI task configuration management easier.
You may be help by interacting with the GitLab API or using a configuration tool like
[Terraform](https://registry.terraform.io/providers/gitlabhq/gitlab/latest).

### Configure a task job

Here is a recommended sample of a CI task job:

```yaml
task:my-scheduled-job:
  # The already defined recommended stage.
  stage: task
  rules:
    # Adjust the CI_SCHEDULE_FREQUENCY variable value according yo your needs.
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CI_SCHEDULE_FREQUENCY == "daily"'
```

Important note: To avoid not desired job being triggered by the scheduled pipeline,
you MUST add preventive rules based on the `CI_PIPELINE_SOURCE` variable like so:

```yaml
# Prevents any execution under a schedule pipeline context.
job1:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never

# Also works because the job is narrowed to a specific pipeline source.
job2:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```

### Configure the schedule pipeline

From your project, go to `CI > Recipes > Schedules`, then create a schedule with the following minimum configuration:

<details><summary>Schedule pipeline configuration sample</summary>

![Schedule a new pipeline](https://gitlab.com/ss-open/ci/recipes/uploads/ac708b34ea4d7eff1420e8ec0fa10e36/image.png)

</details>

Note: The `CI_SCHEDULE_FREQUENCY` variable value MUST match the value you define on your CI job rules.

## Include methods for projects outside of gitlab.com

The include samples above only work if your project is hosted on gitlab.com, not on self-hosted instances.

Several workarounds are described below to use `ci/recipes` on self-hosted instance.

### Using the `remote` include (recommended)

You CAN use the `remote` include instead of `project` but you need to use a specific address:

```yaml
include:
  - remote: https://gitlab.com/api/v4/projects/47154745/packages/generic/ci-recipes/vX.Y.Z/auto.gitlab-ci.yml
```

Replace `vX.Y.Z` by the latest current release.

Note: Unfortunately, automatic update trough renovate is currently not supported.
See: <https://github.com/renovatebot/renovate/discussions/25880>

You MIGHT want to use the `remote` include directive with a direct URL pointing to this project
but it will not works because of the internal `local` include directives that will resolve to
your instance GitLab instance instead of gitlab.com.

### Project mirroring

Note: You MUST have at least the Premium plan of GitLab to have the pull mirroring enabled.

To make these recipes working on your own instance, you MUST create a mirror repository of this one.

Follow the sample step below to achive that.
You MAY also read the [official guide](https://docs.gitlab.com/ee/user/project/repository/mirror/).

#### Create a project for mirroring

1. Go to the project creation page and select "Create blank project".
1. Choose a name for the project. For this example, we will name it `ci-recipes`.
1. Ensure the "Initialize repository with a README" option is **not** activated.
1. Create the project

#### Enabled project mirroring

1. Go to your newly created project page.
1. Select "Settings > Repository".
1. Expand "Mirroring repositories".
1. Select "Add new".
1. Fill the "Git repository URL" input with the following: `ssh://git@gitlab.com:ss-open/ci/recipes.git`
1. Select "Pull" on the "Mirror direction" input. (Premium only)
1. Click on the "Mirror repository" button

#### Use the mirrored repository

Include the configurations the same way but using the previously created mirror repository:

```yaml
include:
  - { project: your-namespace/ci-recipes, ref: vX.Y.Z, file: auto.gitlab-ci.yml }
```

## Create your own custom CI entrypoint

It is possible to create your own CI entrypoint based on our own configuration by creating your own `my-group/ci` project.

Create a CI configuration file different from the default `.gitlab-ci.yml` which is reserved for the project itself.
For example: `main.gitlab-ci.yml`.

Include `ss-open/ci/recipes` with your cross-project configurations on it, then use it by including them the same way:

```yaml
include:
  - { project: my-company/ci, ref: <branch|tag>, file: main.gitlab-ci.yml }
```

It is useful if you have a lot of common override or additional logic to share across you projects
but also as a bridge to resolve include resolution issues on self-hosted gitlab instance.

## Job specific documentation

Some job specific documentation files ara available across the repository, next to their respective files.

Take a look around the [different stages' folders](./stages/) to explore them.

## Troubleshooting

### `!reference ["***"] could not be found` or `unknown keys in extends (***)`

If you have this error, you likely included only some jobs of this project,
or only the Docker workflow part, but forgot to include the
[init.gitlab-ci.yml](init.gitlab-ci.yml)
file.

Please refer to ["Partial include"](#partial-include) section of the documentation.

### The npm vendor cache creation is always being killed

You likely have the following error during the creation of the cache:

```txt
/scripts-xxx/archive_cache: line 205: 200 Killed '/usr/bin/gitlab-runner-helper' cache-archiver --file ...
```

Try to use a runner with bigger available resources (cpu/ram).
If it does not solve the problem, then you MAY need to remove the `node_modules`
from the cache path list of the `node:install` template:

```yaml
.node:install:
  cache:
    # Copied from the original template, but without node_modules
    paths:
      - ${CI_CACHE_PATH}
```

However, you will not get any speed up benefit of the `npm install` script part.
