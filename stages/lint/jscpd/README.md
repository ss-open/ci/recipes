# lint:jscpd

🌐 <https://github.com/kucherenko/jscpd>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/jscpd/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## HTML report

An HTML report of jscpd is available from the [job artifacts](https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html).

Use the `Browse` button of the artifact, then open the `report/html/index.html` file.
It will leads you to the web report of jscpd.

## Run locally

```sh
npx jscpd --gitignore --exitCode
```

⚠️ The `.gitignore` file management, provided by the `--gitignore` option,
actually does not fulfill the git specifications.
You MIGHT have to trick with your project `.gitignore` file
or use the `ignore` option from the JSCPD configuration file.
See the following issue for details: <https://github.com/kucherenko/jscpd/issues/466>

Note: We recommend to use the exact same version as the one defined on the
[related CI configuration](./.gitlab-ci.yml).
