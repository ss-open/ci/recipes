# lint:npm:depcheck

🌐 <https://github.com/depcheck/depcheck>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/npm:depcheck/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Run locally

```sh
npx depcheck
```

Note: We recommend to use the exact same version as the one defined on the
[related CI configuration](./.gitlab-ci.yml).

## Configuration file

You can adjust the report with the `.depcheckrc.yml` file. For example:

```yaml
ignores:
  # Global import
  - '@types/jest'
  # Used on npm scripts
  - 'dotenv'
```

The configuration file can be written in many format as described on the
[documentation](https://github.com/depcheck/depcheck#usage-with-a-configuration-file).

The available options matches the cli ones, defined
[here](https://github.com/depcheck/depcheck#usage).
