# lint:markdownlint

🌐 <https://github.com/DavidAnson/markdownlint>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/markdownlint/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Fixing files

```sh
docker run --rm -v $PWD:/workdir davidanson/markdownlint-cli2 --fix "**/*.{md,markdown}"
```

Note: We recommend to use the exact same docker tag as the one defined on the
[related CI configuration](./.gitlab-ci.yml).

## Documentation

### CLI

<https://github.com/DavidAnson/markdownlint-cli2>

### Rules

<https://github.com/DavidAnson/markdownlint#rules--aliases>
