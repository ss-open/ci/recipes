# lint:tsc

🌐 <https://www.typescriptlang.org/docs/handbook/compiler-options.html>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/tsc/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).
