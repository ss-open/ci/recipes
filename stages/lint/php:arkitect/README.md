# lint:php:arkitect

🌐 <https://github.com/phparkitect/arkitect>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/php:arkitect/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Project configuration

Create the `phparkitect.php` configuration file manually or generate it
trough the following command to automatically have a complete sample:

```sh
docker run --rm -t -v ${PWD}:${PWD} -w ${PWD} registry.gitlab.com/ss-open/ci/images/phparkitect init
```

## Run locally

```sh
docker run --rm -t -v ${PWD}:${PWD} -w ${PWD} registry.gitlab.com/ss-open/ci/images/phparkitect check
```

You MIGHT want to use the `--generate-baseline` option the first time
in order to save the current violations and resolve them later.

Note: To ensure behavior consistency, you MAY use the same docker tag as the
[related CI configuration](./.gitlab-ci.yml).

## Documentation

- Configuration: <https://github.com/phparkitect/arkitect#configuration>
- Available rules: <https://github.com/phparkitect/arkitect#available-rules>
