# lint:shellcheck

🌐 <https://www.shellcheck.net/>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/shellcheck/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Fixing files

For this job, we recommend to verify the failed CI job log output.
Each reported error is listed with its related wiki link at the end.

If you want to run it locally, the project offers [multiple usages and integrations](https://github.com/koalaman/shellcheck#how-to-use).
