# lint:php-code-sniffer

🌐 <https://github.com/squizlabs/PHP_CodeSniffer>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/php-code-sniffer/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Project configuration

Install php codesniffer as a dev project dependency:

```sh
composer require --dev squizlabs/php_codesniffer
```

Create a `phpcs.xml.dist` with the following:

```xml
<?xml version="1.0" encoding="UTF-8"?>

<ruleset
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="vendor/squizlabs/php_codesniffer/phpcs.xsd"
>
  <arg name="basepath" value="." />
  <arg name="cache" value=".phpcs-cache" />
  <arg name="colors" />

  <!--
    Limits to .php extension files only.
    This configuration depends of your project structure
    and the kind of file you want php-code-sniffer being applied to.
    @see https://github.com/squizlabs/PHP_CodeSniffer/wiki/Advanced-Usage#specifying-valid-file-extensions
  -->
  <arg name="extensions" value="php" />

  <!-- Rules sets configuration -->
  <rule ref="PSR12" />

  <!-- Paths to analyze -->
  <file>src/</file>
  <file>tests/</file>
</ruleset>
```

For a complete and annotated configuration sample, please refer to the following:
<https://github.com/squizlabs/PHP_CodeSniffer/wiki/Annotated-Ruleset>

Then, add the cache file path yo your `.gitignore` configuration:

```ignore
/.phpcs-cache
```

## Run locally

To fix the files:

```sh
composer exec phpcbf
```

To see the remaining manual fixes report:

```sh
composer exec phpcs
```

Note: To ensure behavior consistency, you MAY use the same docker tag as the
[related CI configuration](./.gitlab-ci.yml).

## Documentation

<https://github.com/squizlabs/PHP_CodeSniffer#getting-started>
