# lint:shfmt

🌐 <https://github.com/mvdan/sh#shfmt>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/shfmt/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Fixing files

```sh
docker run --rm -w${PWD} -v${PWD}:${PWD} registry.gitlab.com/ss-open/ci/images/shfmt shfmt -w .
```

Replace `X.Y.Z` by the version use by the related CI job.

## Ignoring files from linting

You may need to ignore some files from linting.
Unfortunately, the `.gitignore` file is not considered by this tool,
but you can play with the `.editorconfig` file like so:

```.editorconfig
[{vendor,node_modules}/**]
ignore = true
```

Checkout the [related vendor issue](https://github.com/mvdan/sh/issues/288) for details.
