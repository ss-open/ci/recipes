# lint:php-cs-fixer

🌐 <https://cs.symfony.com/>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/php-cs-fixer/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Project configuration

Create a `.php-cs-fixer.dist.php` configuration to get the job working properly.
A sample is available at the [root directory](/.php-cs-fixer.dist.php) of this project.

Also add the following to your `.gitignore`:

```gitignore
/.php-cs-fixer.cache
/.php-cs-fixer.php
```

## Run locally

```sh
docker run --rm -t -v ${PWD}:${PWD} -w ${PWD} registry.gitlab.com/ss-open/ci/images/php-cs-fixer fix
```

Note: To ensure behavior consistency, you MAY use the same docker tag as the
[related CI configuration](./.gitlab-ci.yml).

## Documentation

You can find the php-cs-fixer documentation on its [official website](https://cs.symfony.com/#documentation).

Here is some hints:

- [How to configure php-cs-fixer](https://cs.symfony.com/doc/config.html#config-file)
- [Rule sets list](https://cs.symfony.com/doc/ruleSets/index.html#list-of-available-rule-sets)
- [Rules list](https://cs.symfony.com/doc/rules/index.html#list-of-available-rules)
