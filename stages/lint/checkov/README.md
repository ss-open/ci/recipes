# lint:checkov

🌐 <https://www.checkov.io/>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/checkov/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Project configuration

To simplify the command line usage, we recommend to initialize first
a `.checkov.baseline` file on your project root folder:

```sh
echo '{}' > .checkov.baseline
```

This file is use to add some report of checkov to be ignored.

## Run locally

```sh
docker run --rm -t -v ${PWD}:${PWD} -w ${PWD} bridgecrew/checkov -d . --baseline .checkov.baseline
```

Including hidden files (default on CI):

```sh
docker run --rm -t -v ${PWD}:${PWD} -w ${PWD} -e CKV_IGNORE_HIDDEN_DIRECTORIES=false \
bridgecrew/checkov -d . --skip-path .git --baseline .checkov.baseline
```

Note: To ensure behavior consistency, you MAY use the same docker tag as the
[related CI configuration](./.gitlab-ci.yml).

### Update the baseline

Some reports may not always be accurate according to the usage context or simply be false positives.

On that case, you can update the previously created baseline file by replacing the `--baseline` by `--create-baseline`:

```sh
docker run --rm -t -v ${PWD}:${PWD} -w ${PWD} bridgecrew/checkov -d . --create-baseline
```

Including hidden files (default on CI):

```sh
docker run --rm -t -v ${PWD}:${PWD} -w ${PWD} -e CKV_IGNORE_HIDDEN_DIRECTORIES=false \
bridgecrew/checkov -d . --skip-path .git --create-baseline
```

⚠️ Ensure to fix the legit reports **before** updating the baseline.

## Documentation

You can find the checkov documentation on its [official website](https://www.checkov.io).

Here is some hints:

- [CLI reference](https://www.checkov.io/2.Basics/CLI%20Command%20Reference.html)
- [Policy index](https://www.checkov.io/5.Policy%20Index/all.html)
