# lint:editorconfig

🌐 <https://editorconfig-checker.github.io>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/editorconfig/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Project configuration

In order to get the job working, you have to create a `.editorconfig` file on your project root directory.

Learn more about the file format and tool integrations on the
[official website of editorconfig](https://editorconfig.org/).

Our recommended configuration can be found with a quick install instruction on the
[ss-open/editorconfig](https://gitlab.com/ss-open/editorconfig) project.

## Run locally

```sh
docker run --rm --volume=${PWD}:/check mstruebing/editorconfig-checker
```

Documentation reference: <https://editorconfig-checker.github.io/#docker>
