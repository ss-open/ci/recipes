# lint:ls-lint

🌐 <https://ls-lint.org/>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/ls-lint/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Project configuration

In order to get this job working, you MUST provide a `.ls-lint.yml` to define the file naming rules.
It MAY be empty if you want to configure it later, but it makes the job useless.

To learn more about this configuration file, please refer to its
[official documentation](https://ls-lint.org/2.0/configuration/the-basics.html#the-basics).

## Run locally

```sh
npx @ls-lint/ls-lint
```

Note: To ensure behavior consistency, you MAY use the same package version as the
[related CI configuration](./.gitlab-ci.yml).
