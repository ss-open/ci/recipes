# lint:eslint

🌐 <https://eslint.org/>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/eslint/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Project configuration

Install eslint as a dev project dependency:

```sh
npm install --save-dev eslint
```

Create a `.eslintrc.js` (or `.cjs`) file on your project root directory. Here is a basic sample:

```javascript
module.exports = {
  extends: ['eslint:recommended'],
  env: {
    node: true,
  },
};
```

## Fixing files

```sh
npx eslint --fix --max-warnings=0 .
```

Note: Some error needs to be manually fixed.

### Ignore files consideration

By default, eslint scans any files present on the project folder except `node_modules`.
It may cause performance issues (e.g. vendor folder scanning) with not desired changed.

This can be worked around by using the `--ignore-path=.gitignore` option.
Here is a sample with `.gitignore`:

```sh
npx eslint --fix --max-warnings=0 --ignore-path=.gitignore .
```

Note: It is not needed to ignore

## Documentation

<https://eslint.org/docs/latest/>
