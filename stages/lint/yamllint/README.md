# lint:yamllint

🌐 <https://yamllint.readthedocs.io>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/yamllint/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Run locally

```sh
docker run --rm -w${PWD} -v${PWD}:${PWD} registry.gitlab.com/ss-open/ci/images/yamllint .
```

Note: To ensure behavior consistency, you MAY use the same docker tag as the
[related CI configuration](./.gitlab-ci.yml).
