# lint:prettier

🌐 <https://prettier.io>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/prettier/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Fixing files

In order to let you override them with ease trough the configuration file, our default options are define on the cli command.

To fix the formatting, we recommend to copy/paste the script line from
the related CI job and replace the `--check` option by `--write`.

## Configuration override

Thanks to the `--config-precedence=file-override` option,
you can override our option by creating a configuration file onto your project root directory.

The file may have multiple name and formats and is documented [here](https://prettier.io/docs/en/configuration.html).
