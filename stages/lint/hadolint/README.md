# lint:hadolint

🌐 <https://github.com/hadolint/hadolint>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/hadolint/.gitlab-ci.yml }

variables:
  SS_CI_RECIPES_HADOLINT_THRESHOLD: style # optional
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Fixing files

Because of the
[lack of directory support](https://github.com/hadolint/hadolint/issues/972),
using the same command as the CI script
combined with the `docker run` instruction can be complex.

However, you can check a single file manually as follow:

```sh
docker run --rm -v ${PWD}:${PWD} -w ${PWD} hadolint/hadolint hadolint --failure-threshold style Dockerfile
```

We recommend instead running the command as described in the
[documentation](https://github.com/hadolint/hadolint#how-to-use)
to the reported dockerfile.

Any reported error is identified by a code and is referenced
[here](https://github.com/hadolint/hadolint/wiki).
