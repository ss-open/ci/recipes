# lint:npm:sort

🌐 <https://github.com/keithamus/sort-package-json>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/npm:sort/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Fixing files

For most of the project, the following command is enough to sort your main and only `package.json` file:

```sh
npx sort-package-json package.json --write
```

If you need to sort multiple `package.json` files, you can do it like so:

```sh
npx sort-package-json "**/package.json" --write
```

However, you have to take into account that tool is not considering the `.gitignore` file
and will can any `package.json` file present on the `node_modules` folder of your project.

Note: We recommend to use the exact same version as the one defined on the
[related CI configuration](./.gitlab-ci.yml).
