# lint:stylelint

🌐 <https://stylelint.io/>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/stylelint/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

### Custom file extensions matching

By default, this job matches on some css file related extensions, defined on the [job itself](./.gitlab-ci.yml).

If your project uses styling inside javascript files you MUST override the variable to include them:

```yaml
lint:stylelint:
  variables:
    # To be adjusted according to your needs.
    SS_CI_RECIPES_STYLELINT_EXTS: css,js,vue
```

## Project configuration

Install stylelint as a dev project dependency:

```sh
npm install --save-dev stylelint stylelint-config-standard
```

Create a `.stylelintrc.{cjs,js,json,yaml,yml}` file on your project root directory. Here is a basic sample:

```javascript
module.exports = {
  extends: ['stylelint-config-standard'],
};
```

## Fixing files

```sh
npx stylelint --ignore-path=.gitignore --fix "**/*.{css,scss,less}"
```

You will need to adjust the extension list according to your project configuration.

Note: Some error needs to be manually fixed.

## Documentation

<https://stylelint.io/>
