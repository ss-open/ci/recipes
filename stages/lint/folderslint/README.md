# lint:folderslint

🌐 <https://github.com/denisraslov/folderslint>

Disclaimer: The tool's documentation specifies it is for front-end projects.
This is actually not true as directory structure constraints can concern any kind of project,
so this tool can be applied anywhere.

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/folderslint/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Project configuration

In order to get this job working, you MUST provide a `.folderslintrc` to define the directory structure rules.
It MUST at least contains the following:

```json
{
  "rules": []
}
```

The `rules` have to be defined by yourself according to the project directory structure you need.

Learn more about the `rules` syntax on the
[official documentation](https://github.com/denisraslov/folderslint#rules-syntax).

## Run locally

```sh
npx folderslint
```

⚠️ If you don't specify the `root` folder configuration key,
the tool will look at all the directories, not tracked included (e.g. vendor).
To workaround that, specify the problematic paths manually
or cleanup your project using git `clean -fdX`.

Note: We recommend to use the exact same version as the one defined on the
[related CI configuration](./.gitlab-ci.yml).
