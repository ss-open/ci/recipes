# lint:rectorphp

🌐 <https://getrector.com/>

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/lint/rectorphp/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).

## Project configuration

Install rector as a dev project dependency:

```sh
composer require --dev rector/rector
```

Create a `rector.php` configuration file with the following minimal configuration:

```php
<?php

declare(strict_types=1);

use Rector\Caching\ValueObject\Storage\FileCacheStorage;
use Rector\Config\RectorConfig;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->cacheClass(FileCacheStorage::class);
    // Needed to get the cache working trough the related CI job.
    // @see https://github.com/rectorphp/rector/issues/8335
    $cacheDir = getenv('CI_CACHE_PATH') ?: '.cache';
    $rectorConfig->cacheDirectory($cacheDir.'/rector');

    // ...
};
```

See also [this project configuration](../../../rector.php) for a complete working sample.

## Run locally

To fix the file by applying the Rector migrations, run the following command:

```sh
composer exec rector
```

## Documentation

<https://getrector.com/documentation>
