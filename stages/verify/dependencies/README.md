# verify:dependencies

This job ensures you are not referencing any unstable release
of an internal dependency across your GitLab group namespace.

It forces you to respect the multi-project release chaining which is
a good practice to avoid accidental use of not yet approved code onto production.

It recognizes an unstable dependency by the release format including a commit reference (e.g. `0.0.0-36wde39r`).
This release format is created by the `publish:review` merge request's job.
In other terms, you MIGHT not use the publish stage to get this job working
but you HAVE TO follow the unstable release publishing format.

## CI configuration

```yaml
include:
  - { project: ss-open/ci/recipes, ref: vX.Y.Z, file: stages/verify/dependencies/.gitlab-ci.yml }
```

Automatically included by [auto.gitlab-ci.yml](auto.gitlab-ci.yml).
