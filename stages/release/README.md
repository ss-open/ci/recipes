# The release stage

The release stage provides automatic semantic releasing following the
[conventional commits](https://www.conventionalcommits.org/en/v1.0.0/)
standard.

You can see a result sample on this project [releases page](https://gitlab.com/ss-open/ci/recipes/-/releases).

## `release:commitlint`

Triggered only on merge request, it use
[`commitlint`](https://commitlint.js.org)
to verify your commits history respects the convention.

If the jobs is failing, the explanation of why is described on its logs.

## `release:create`

Triggered at the very end of the main branch pipeline,
this job is responsible of the next release creation when its needed.

It is backed by the [`semantic-release`](https://semantic-release.gitbook.io) tool
combined with its [gitlab plugin](https://github.com/semantic-release/gitlab).

Note: If you have a project publishing a NPM package, it won't be done by this job.
The release creation will only create the tag and its associated GitLab release.
The NPM package creation will be managed by the next production pipeline based on the freshly created tag.
