<?php

declare(strict_types=1);

use Rector\Caching\ValueObject\Storage\FileCacheStorage;
use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\SetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->cacheClass(FileCacheStorage::class);
    // Needed to get the cache working trough the related CI job.
    // @see https://github.com/rectorphp/rector/issues/8335
    $cacheDir = getenv('CI_CACHE_PATH') ?: '.cache';
    $rectorConfig->cacheDirectory($cacheDir.'/rector');

    // User configuration, to be adapted according to your project structure.
    $rectorConfig->paths([
        __DIR__ . '/fixtures',
    ]);
    $rectorConfig->sets([
        SetList::TYPE_DECLARATION,
    ]);
};
