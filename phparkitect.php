<?php

declare(strict_types=1);

use Arkitect\ClassSet;
use Arkitect\CLI\Config;
use Arkitect\Expression\ForClasses\ResideInOneOfTheseNamespaces;
use Arkitect\Rules\Rule;
use Arkitect\Expression\ForClasses\IsNotFinal;

return static function (Config $config): void {
    $classSet = ClassSet::fromDir(__DIR__.'/fixtures');

    $rules[] = Rule::allClasses()
        ->that(new ResideInOneOfTheseNamespaces('Fixture'))
        ->should(new IsNotFinal())
        ->because('reasons')
    ;

    $config->add($classSet, ...$rules);
};
