module.exports = {
  extends: ['stylelint-config-standard'],
  rules: {
    "unit-allowed-list": ["em", "rem"]
  }
};
