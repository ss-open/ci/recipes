<?php

namespace Fixture;

class Fixture
{
    public function run(bool $param): int|string
    {
        if ($this->isTrue($param)) {
            return 5;
        }

        return '10';
    }

    private function isTrue(bool $value): bool
    {
        return $value === true;
    }
}
